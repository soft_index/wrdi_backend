<?php

namespace App\Admin\Controllers;

use App\Models\Country;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
Use Encore\Admin\Widgets\Table;
class CountryController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Country';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Country());

        $grid->column('id', __('Id'));
        $grid->column('name', 'Name')->expand(function ($model) {

            $variations = $model->cities()->get()->map(function ($variation) {
                return $variation->only(['id','name','latitude','longitude']);
            });

            return new Table(['ID','Name','Latitude','Longitude'], $variations->toArray());
        });
        $grid->column('dial_code', __('Dial code'));
        $grid->column('latitude', __('Latitude'));
        $grid->column('longitude', __('Longitude'));
        $grid->column('status', __('Status'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Country::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('dial_code', __('Dial code'));
        $show->field('latitude', __('Latitude'));
        $show->field('longitude', __('Longitude'));
        $show->field('status', __('Status'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Country());

        $form->tab('Country', function ($form) {
            $form->text('name', __('Name'))->required();
            $form->text('dial_code', __('Dial code'))->required();
            $form->text('latitude', __('Latitude'));
            $form->text('longitude', __('Longitude'));
            $form->switch('status', __('Status'))->default(1);

        })->tab('Cities', function ($form) {
            $form->hasMany('cities', function (Form\NestedForm $form) {
                $form->text('name', __('Name'))->required();
                $form->text('latitude', __('Latitude'));
                $form->text('longitude', __('Longitude'));
                $form->switch('status', __('Status'))->default(1);

            });
        });
        return $form;
    }
}
