<?php

namespace App\Admin\Controllers;

use App\Models\OrderStatus;
use App\Models\Product;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

use App\Models\User;
use DB;
use Carbon\Carbon;
class HomeController extends AdminController
{
    protected $title = 'Dashboard';
    public function grid()
    {
        $users=User::whereBetween('created_at',
            [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]
        )->count();
        $products=Product::all()->count();
        $status=OrderStatus::where('id','!=','5')->get();
        return view('admin.dashboard',compact('users','products','status'));
    }
}
