<?php

namespace App\Admin\Controllers;

use App\Models\Cart;
use App\Models\Order;
use App\Models\Product;
use App\Models\UserAddress;
use Carbon\Carbon;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
Use Encore\Admin\Widgets\Table;
use Illuminate\Support\Facades\DB;

class MostPurchasedController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Most Purchased';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $from=request()->input('start');
        $to=request()->input('end');


        $orderIds=Order::all();

        if((request()->has('start')) && request()->has('end'))
        {

            if(request()->filled('start') && request()->filled('end'))
            {
                $orderIds=Order::whereRaw(
                "(created_at >= ? AND created_at <= ?)",
                [
                    $from ." 00:00:00",
                    $to ." 23:59:59"
                ]
            )->get();

            }
        }


        $orderIds=$orderIds->pluck('id');
        $address=DB::table('order_address')->whereIn('order_id',$orderIds)->pluck('address_id');
        $orders=UserAddress::whereIn('id',$address)->select('city', DB::raw('count(*) as total'))
            ->groupBY('city')->orderBy('total','DESC')->get();

        return view('reports.most-purchased',compact('from','to','orders'));
    }


}
