<?php

namespace App\Admin\Controllers;

use App\Models\Cart;
use App\Models\Product;
use Carbon\Carbon;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
Use Encore\Admin\Widgets\Table;
use Illuminate\Support\Facades\DB;

class MostValuableController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Most Valuable';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $from=request()->input('start');
        $to=request()->input('end');
        $product_id=request()->input('product');
        $products=Product::all();

        $orders=Cart::where('order_status',1);


        if((request()->has('start')))
        {

            $orders=$orders->when(request()->filled('start'), function ($data)  use($from) {
                return $data->where('created_at', '>=', $from);
            });
        }
        if(request()->input('end'))
        {
            if(request()->input('start') != request()->input('end'))
            {
                $orders=$orders->when(request()->filled('start'), function ($data)  use($to) {
                    return $data->where('created_at', '<=', $to);
                });
            }

        }


            $orders=$orders->select('product_id', DB::raw('count(*) as total_order'), DB::raw('sum(total_amount) as total'))
                ->groupBy('product_id')->get();

        return view('reports.most-valuable',compact('from','to','products','orders','product_id'));
    }


}
