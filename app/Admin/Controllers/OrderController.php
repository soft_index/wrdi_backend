<?php

namespace App\Admin\Controllers;

use App\Models\Order;
use App\Models\OrderCancel;
use App\Models\OrderStatus;
use App\Models\Rider;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class OrderController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Order';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Order());

        $grid->column('id', __('Id'));
        $grid->column('order_no')->display(function ($order_no) {

            return "<a href='/admin/orders/$this->id'>$this->order_no</a>";

        });
        $grid->user()->name()->label();
        $grid->items()->display(function ($items) {

            $items = array_map(function ($item) {
                return "<span class='label label-success'>{$item['product']['title']}</span>";
            }, $items);

            return join('&nbsp;', $items);
        });
        $grid->column('net_amount', __('Net amount'));
        $grid->column('discount', __('Discount'));
        $grid->column('total_amount', __('Total amount'));
        $grid->column('order_date', __('Order date'));
        $grid->status()->title('status')->label();
        /*
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));*/

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $order=Order::find($id);
        $status=OrderStatus::where('id','!=','5')->get();
        $riders=Rider::all();
        $rider_order=$order->rider()->first();
        $cancel=OrderCancel::where('order_id',$order->id)->first();
        return view('admin.invoice',compact('order','status','id','riders','rider_order','cancel'));
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Order());

        $form->number('user_id', __('User id'));
        $form->text('net_amount', __('Net amount'));
        $form->text('discount', __('Discount'));
        $form->text('total_amount', __('Total amount'));
        $form->datetime('order_date', __('Order date'))->default(date('Y-m-d H:i:s'));
        $form->number('status_id', __('Status id'));

        return $form;
    }
}
