<?php

namespace App\Admin\Controllers;

use App\Models\Product;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
Use Encore\Admin\Widgets\Table;
use Illuminate\Support\Facades\Auth;

class ProductController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Product';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $user=Auth::guard('admin')->user();
        $role=$user->roles;
        $role_name=$role[0]->name;

        $grid = new Grid(new Product());

        $grid->column('id', __('Id'));
        $grid->column('code', __('Code'));
        $grid->column('title', 'title')->expand(function ($model) {

            $variations = $model->variations()->get()->map(function ($variation) {
                return $variation->only(['id','title','price','quantity']);
            });

            return new Table(['ID','title','price','quantity'], $variations->toArray());
        });
        $grid->column('description', __('Description'));
        $grid->column('price', __('Price'));
        $grid->column('image', __('Image'));
        $grid->column('status', __('Status'));
        $grid->column('quantity', __('Quantity'));
        $grid->column('generic_name', __('Generic name'));
        $grid->column('weight', __('Weight'));
        $grid->column('height', __('Height'));
        $grid->column('width', __('Width'));
        $grid->column('length', __('Length'));
        if($role_name==='Warehouse Manager')
        {
            $grid->disableCreateButton();

            $grid->actions(function ($actions) {
                $actions->disableView();
                $actions->disableDelete();





            });
        }
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Product::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('code', __('Code'));
        $show->field('title', __('Title'));
        $show->field('description', __('Description'));
        $show->field('price', __('Price'));
        $show->field('image', __('Image'));
        $show->field('status', __('Status'));
        $show->field('quantity', __('Quantity'));
        $show->field('generic_name', __('Generic name'));
        $show->field('weight', __('Weight'));
        $show->field('height', __('Height'));
        $show->field('width', __('Width'));
        $show->field('length', __('Length'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $user=Auth::guard('admin')->user();
        $role=$user->roles;
        $role_name=$role[0]->name;

        $form = new Form(new Product());
        if($role_name==='Warehouse Manager')
        {
            $form->tab('Basic Info', function ($form) {
                $form->hasMany('languages', function (Form\NestedForm $form) {
                    $form->text('title')->readonly();
                    $form->text('description')->readonly();
                    $form->select('locale')->options(['en' => 'En','ar' =>'Ar'])->readonly();

                });

            })->tab('product Details', function ($form) {
                $form->text('code', __('Code'))->readonly();
                $form->text('price', __('Price'))->readonly();
                $form->image('image', __('Image'))->readonly();
                $form->switch('status', __('Status'))->default(1)->readonly();;
                $form->number('quantity', __('Quantity'));
                $form->text('generic_name', __('Generic name'))->readonly();;
                $form->text('weight', __('Weight'))->readonly();
                $form->text('height', __('Height'))->readonly();
                $form->text('width', __('Width'))->readonly();
                $form->text('length', __('Length'))->readonly();
                $form->date('expiry', __('Expiration'))->format('YYYY-MM-DD');

            })->tab('Variations', function ($form) {
                $form->hasMany('variations', function (Form\NestedForm $form) {
                    $form->text('title')->readonly();
                    $form->text('price')->default(0)->readonly();
                    $form->image('image')->readonly();
                    $form->number('priority')->readonly();
                    $form->number('quantity')->readonly();
                    $form->switch('status', __('Status'))->default(1)->readonly();
                });
            });

            $form->tools(function (Form\Tools $tools) {
                $tools->disableDelete();
                $tools->disableView();


            });
        }
        else
        {
            $form->tab('Basic Info', function ($form) {
                $form->hasMany('languages', function (Form\NestedForm $form) {
                    $form->text('title');
                    $form->text('description');
                    $form->select('locale')->options(['en' => 'En','ar' =>'Ar']);

                });

            })->tab('product Details', function ($form) {
                $form->text('code', __('Code'));
                $form->text('price', __('Price'));
                $form->image('image', __('Image'));
                $form->switch('status', __('Status'))->default(1);
                $form->number('quantity', __('Quantity'));
                $form->text('generic_name', __('Generic name'));
                $form->text('weight', __('Weight'));
                $form->text('height', __('Height'));
                $form->text('width', __('Width'));
                $form->text('length', __('Length'));
                $form->date('expiry', __('Expiration'))->format('YYYY-MM-DD');

            })->tab('Variations', function ($form) {
                $form->hasMany('variations', function (Form\NestedForm $form) {
                    $form->text('title');
                    $form->text('price')->default(0);
                    $form->image('image');
                    $form->number('priority');
                    $form->number('quantity');
                    $form->switch('status', __('Status'))->default(1);
                });
            });
        }







        return $form;
    }
}
