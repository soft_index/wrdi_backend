<?php

namespace App\Admin\Controllers;

use App\Models\Cart;
use App\Models\Product;
use Carbon\Carbon;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
Use Encore\Admin\Widgets\Table;

class ProductSalesController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Product Sales';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $from=request()->input('start');
        $to=request()->input('end');
        $product_id=request()->input('product');
        $products=Product::all();

        $order=Cart::where('order_status',1);
        $orders=$order->get();

        if($product_id && $product_id != 0)
        {

            $orders=$orders->when(request()->filled('product'), function ($data)  use($product_id) {
                return $data->where('product_id',$product_id);
            });
        }
        if((request()->has('start')))
        {

            $orders=$orders->when(request()->filled('start'), function ($data)  use($from) {
                return $data->where('created_at', '>=', $from);
            });
        }
        if(request()->input('end'))
        {
            if(request()->input('start') != request()->input('end'))
            {
                $orders=$orders->when(request()->filled('start'), function ($data)  use($to) {
                    return $data->where('created_at', '<=', $to);
                });
            }

        }
        else
        {

            $orders=$orders->where('created_at', '>=', Carbon::today()->toDateString());
        }



        return view('reports.productsales',compact('from','to','products','orders','product_id'));
    }


}
