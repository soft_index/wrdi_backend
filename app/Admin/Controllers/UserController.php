<?php

namespace App\Admin\Controllers;

use App\Models\User;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class UserController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'User';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new User());
        $grid->model()->withoutGlobalScopes();
        $grid->column('id', __('Id'));
        $grid->column('name', __('Name'));
        $grid->column('email', __('Email'));
        $grid->column('phone', __('Phone'));
        $grid->column('created_at', __('Created at'));


        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(User::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('email', __('Email'));
        $show->field('phone', __('Phone'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));


        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {

        $form = new Form(new User());
        if($form->isEditing())
        {
            $form->model()->withoutGlobalScopes();
        }
        $form->text('name', __('Name'))->rules('required');
        $form->email('email', __('Email')) ->creationRules(['required', "unique:users"])
            ->updateRules(['required', "unique:users,email,{{id}}"]);
        $form->mobile('phone', __('Phone'))->rules('required');
        $form->password('password', trans('Password'))->rules('required|confirmed')->default(function ($form) {
            return $form->model()->password;
        });

        $form->password('password_confirmation', trans('Confirm Password'))->rules('required')
            ->default(function ($form) {
                return $form->model()->password;
            });

        $form->ignore(['password_confirmation']);
        $form->radio('status','Status')->options([1=>'Active',0=>'Disable']);
        return $form;
    }
}
