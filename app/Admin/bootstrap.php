<?php


use Encore\Admin\Facades\Admin;
use Encore\Admin\Grid;

Admin::js('/vendor/laravel-admin/selectbox/bootstrap-select.min.js');
Admin::css('/vendor/laravel-admin/selectbox/bootstrap-select.min.css');

Encore\Admin\Form::forget(['map', 'editor']);

Grid::init(function (Grid $grid) {


    $grid->disableColumnSelector();


    $grid->disableExport();


});
