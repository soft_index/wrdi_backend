<?php

use Illuminate\Routing\Router;

Admin::routes();
Admin::registerAuthRoutes();
Route::resource('admin/auth/users', \App\Admin\Controllers\CustomUserController::class)->middleware(config('admin.route.middleware'));

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    $router->resource('users', UserController::class);
    $router->resource('products', ProductController::class);
    $router->resource('orders', OrderController::class);
    $router->resource('order-statuses', OrderStatusController::class);
    $router->resource('riders', RiderController::class);
    $router->resource('reviews', ReviewController::class);
    $router->resource('product-sales', ProductSalesController::class);
    $router->resource('most-valuable', MostValuableController::class);
    $router->resource('most-purchased', MostPurchasedController::class);
    $router->resource('countries', CountryController::class);



});
