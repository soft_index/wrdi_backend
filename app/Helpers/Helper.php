<?php

use Illuminate\Support\Facades\URL;

if (!function_exists('includeRouteFiles')) {

/**
* Loops through a folder and requires all PHP files
* Searches sub-directories as well.
*
* @param $folder
*/
function includeRouteFiles($folder)
{
$directory = $folder;
$handle = opendir($directory);
$directory_list = [$directory];

while (false !== ($filename = readdir($handle))) {
if ($filename != '.' && $filename != '..' && is_dir($directory.$filename)) {
array_push($directory_list, $directory.$filename.'/');
}
}

foreach ($directory_list as $directory) {
foreach (glob($directory.'*.php') as $filename) {
require $filename;
}
}
}
}

if (!function_exists('paginate')) {

    function paginate($data,$type)
    {
        if(Request::has('offset') || Request::has('limit'))
        {
            $query=$data->when(Request::has('offset'), function ($data)  {
                return $data->offset(Request::input('offset'));
            })->when(Request::has('limit'), function ($data)  {
                return $data->limit(Request::input('limit'));
            })->get();
            $success[$type] = $query;
            $success['total'] = $data->count();
            return $success;

        }
        else
            $data->paginate(10);

    }
}
if (!function_exists('arrayPagination')) {

    function arrayPagination($items,$type, $perPage = 6, $page = null, $options = [])
    {
        $total=$items->count();
        if(Request::has('offset') && Request::has('limit'))
        {
            $query= $items->slice(Request::input('offset'),Request::input('limit'));

            if (is_array($query))
            {
                $success[$type] = $query;
            } else {
                $success[$type] = $query->values();
            }

            $success['total'] = $total;
            return $success;
        }
        else
        {
            $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
            $items = $items instanceof \Illuminate\Support\Collection ? $items : Collection::make($items);
            return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
        }



    }

}

