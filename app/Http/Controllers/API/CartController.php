<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Product;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    use ApiResponser;

    public function addtoCart(Request $request)
    {
        $request->validate([
            'cartItems.*.product_id' => 'required|integer',
            'cartItems.*.quantity' => 'required|integer',
            'cartItems.*.total_amount' => 'required',

        ]);
        $user=auth()->user();
        if(is_array($request->cartItems))
        {
            foreach($request->cartItems as $cartItem)
            {

                $product=Product::find($cartItem['product_id']);
                if($product)
                {

                    $cart=Cart::create(

                    [
                        'product_id'=>$product->id,
                        'user_id' => $user->id,
                        'order_status' => 0,
                    'quantity' =>$cartItem['quantity'],
                    'total_amount' =>$product->price*$cartItem['quantity'],
                    ]);

                    if(isset($cartItem['variation_id']))
                    {
                        DB::table('cart_variations')
                            ->where('cart_id',$cart->id)->where('variation_id',$cartItem['variation_id'])->delete();
                        DB::table('cart_variations')->insert([
                            'cart_id'=>$cart->id,
                            'variation_id'=>$cartItem['variation_id']
                        ]);
                    }

                }
                else
                    return $this->error($cartItem['product_id'].' Wrong product item!',404);

            }
            return $this->success($user->cart,'Items added to cart successfully!');
        }
        else
            return $this->success(null,'Please select the cart item!');


    }

    public function getCart()
    {
        $user=auth()->user();
        $cart=Cart::where('user_id',$user->id)->where('order_status',0)->orderBy('created_at','DESC')->get();

        return $this->success($cart,'Cart items!');
    }
    public function removeAll()
    {
        $user=auth()->user();
        $cart=Cart::where('user_id',$user->id)->where('order_status',0)->delete();

        return $this->success(null,'Cart empty!');
    }
    public function removeitem($id)
    {
        $user=auth()->user();
        $cart=Cart::where('user_id',$user->id)->where('id',$id)->where('order_status',0)->delete();

        return $this->success(null,'Item remove from cart!');
    }
}
