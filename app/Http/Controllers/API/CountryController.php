<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    use ApiResponser;

    public function countryList()
    {
        $country=Country::with('cities')->get();
        return $this->success($country,'Country with cities');
    }
}
