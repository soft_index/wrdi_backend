<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\FavoriteProduct;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class FavoriteController extends Controller
{
    use ApiResponser;

    public function action(Request $request)
    {
        $input = $request->validate([
            'product_id' => 'required|integer',
            'action' => 'required|boolean',
        ]);
        $user=auth()->user();
        $action=$input['action'];
        if($action)
        {
            FavoriteProduct::updateorCreate([
                'user_id' => $user->id,
                'product_id'=> $input['product_id']
            ]);

            return $this->success(null,'Favorite list updated.');
        }
        else
            FavoriteProduct::find($input['product_id'])->delete();
            return $this->success(null,'Product remove from favorite list.');
    }

    public function myFavoriteList()
    {
        $user=auth()->user();
        $data=$user->favorities()->orderBy('created_at','DESC');
        $result=paginate($data);
        return $this->success($result,'My favorite list.');
    }
}
