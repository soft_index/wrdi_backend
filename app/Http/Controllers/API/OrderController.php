<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\Product;
use App\Models\ProductVariation;
use App\Models\UserAddress;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    use ApiResponser;

    public function createOrder(Request $request)
    {
        $user=auth()->user();
        $total=0;
        $net_total=0;
        $discount=0;
        $variation_total=0;
        $prefix="WR-".date("y")."-".date("m")."-";

        $input=$request->validate([
            'items.*.product_id' => 'required|integer',
            'items.*.quantity' => 'required|integer',
            'items.*.total_amount' => 'required',
            'date'=>'required',
            'discount'=>'required',
            'shipping_address'=>'required'
        ]);
        $user=auth()->user();
        $address=$request->shipping_address;
        $address_id=$request->address_id;
        if($address)
        {
            $address_id=$this->userAddress($user->id,$address);
        }

        if(is_array($request->items))
        {
            foreach($request->items as $item)
            {
                $product=Product::find($item['product_id']);
                if($product)
                {
                    $cart=Cart::create(
                        [
                            'product_id'=>$product->id,
                            'user_id' => $user->id,
                            'order_status' => 0,
                            'quantity' =>$item['quantity'],
                            'total_amount' =>$product->price*$item['quantity'],
                        ]);
                    if(isset($item['variation_id']))
                    {
                        $variation=ProductVariation::find($item['variation_id']);
                        if($variation)
                        {
                            DB::table('cart_variations')
                                ->where('cart_id',$cart->id)->where('variation_id',$item['variation_id'])->delete();
                            DB::table('cart_variations')->insert([
                                'cart_id'=>$cart->id,
                                'variation_id'=>$item['variation_id'],
                                'total' =>$variation->price
                            ]);
                        }
                    }
                }
                else
                    return $this->error($item['product_id'].' Wrong product item!',404);

            }

        }

        $carts=$user->cart->pluck('id');
        $discount=$request->input('discount');
        $cartItems=Cart::whereIn('id',$carts)->where('user_id',$user->id)->where('order_status',0)->get();
        if(count($cartItems) > 0)
        {
            $net_total=Cart::whereIn('id',$carts)->where('user_id',$user->id)->where('order_status',0)->sum('total_amount');
            $variation_total=DB::table('cart_variations')->whereIn('cart_id',$carts)->sum('total');
            $net_total+=$variation_total;
            $last_order=Order::orderBy('id','DESC')->first();
            $number=str_pad($last_order->id + 1, 4, "0", STR_PAD_LEFT);
            $order=new Order();
            $order->user_id=$user->id;
            $order->net_amount=$net_total;
            $order->discount=$discount;
            $order->total_amount=$net_total-$discount;
            $order->order_date=Carbon::parse($request->input('date'));
            $order->status_id=OrderStatus::where('title','Pending')->value('id');
            $order->order_no=$prefix.$number;
            $order->save();
            foreach($carts as $cart)
            {
                DB::table('order_items')->insert([
                    'order_id' => $order->id,
                    'cart_id' => $cart
                ]);
            }
            Cart::whereIn('id',$carts)->where('user_id',$user->id)->where('order_status',0)->update(['order_status'=>1]);
            $order=Order::with('items','address')->where('id',$order->id)->first();
            if($address_id)
            {
                DB::table('order_address')->insert([
                    'order_id' => $order->id,
                    'address_id' => $address_id,
                    'created_at'=> $order->created_at,
                    'updated_at'=> $order->created_at,

                ]);
            }

            return $this->success($order,'Order placed successfully!');
        }
        else
            return $this->error('No cart item found',404);

    }
    public function getMyOrders()
    {
        $user=auth()->user();
        $orders=Order::with('items','address')->where('user_id',$user->id)->orderBy('created_at','DESC');
        $data=paginate($orders,'orders');
        return $this->success($data,'My orders!');
    }

    public function getOrder(Request $request)
    {
        $user=auth()->user();
        $id=$request->route('id');
        $orders=Order::with('items')->where('user_id',$user->id)->where('id',$id)->first();
        return $this->success($orders,'My single order!');
    }
    public function addOrderStatus(request $request)
    {
        $input=$request->validate([
            'title' =>'required',
            'priority' =>'required|integer',

        ]);

        $status=OrderStatus::create($input);

        return $this->success($status,'Status added successfully!');
    }
    public function getAllStatus()
    {
        $status=OrderStatus::all();

        return $this->success($status,'All Statuses!');
    }
    public function userAddress($user_id,$address)
    {

        if($address)
        {
            $orderAddress=new UserAddress();
            $orderAddress->user_id=$user_id;
            if(isset($address['name']))
            {
                $orderAddress->name=$address['name'];
            }
            if(isset($address['address']))
            {
                $orderAddress->address=$address['address'];
            }
            if(isset($address['city']))
            {
                $orderAddress->city=$address['city'];
            }
            if(isset($address['zip_code']))
            {
                $orderAddress->zip_code=$address['zip_code'];
            }
            if(isset($address['latitude']))
            {
                $orderAddress->latitude=$address['latitude'];
            }
            if(isset($address['longitude']))
            {
                $orderAddress->longitude=$address['longitude'];
            }
            if(isset($address['address_title']))
            {
                $orderAddress->address_title=$address['address_title'];
            }

            $orderAddress->save();

            return $orderAddress->id;
        }
    }
}
