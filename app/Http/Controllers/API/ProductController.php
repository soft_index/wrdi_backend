<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductVariation;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    use ApiResponser;

    public function store(Request $request)
    {
        $input = $request->validate([
            'title' => 'required',
            'description' => ['required'],
            'price' => ['required'],
            'code' => ['required'],
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'quantity' => ['required'],

        ]);
        $imageName = time().'.'.$request->image->extension();

        $request->image->move(public_path('images'), $imageName);

        $input['image']=$imageName;
        $product = Product::create($input);

        return $this->success($input,'Product added successfully!');
    }

    public function getProduct(Request $request)
    {

        $search = $request->input('search');
        $product=Product::where('status',1);
        if($search)
        {
            $collection=$product->get();
            $collectionOne = $collection->filter(function ($item) use ($search) {
                return strpos($item->title, $search) !== false;
            });
            $collectionTwo = $collection->filter(function ($item) use ($search) {
                return strpos($item->description, $search) !== false;
            });
            $collectionThree = $collection->filter(function ($item) use ($search) {
                return strpos($item->generic_name, $search) !== false;
            });
            $merge=$collectionOne->merge($collectionTwo);
            $merge=$merge->merge($collectionThree);
            $count=count($merge);
            $data=arrayPagination($merge,'products',$count);
            return $this->success( $data,'Product list!');
        }
        $data=paginate($product,'products');

        return $this->success( $data,'Product list!');

    }

    public function storeVariation(Request $request)
    {
        $input = $request->validate([
            'product_id' => 'required|integer',
            'title' => 'required',
            'price' => 'required',
            'priority' => 'required|integer',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);
        if($request->has('image'))
        {
            $imageName = time().'.'.$request->image->extension();

            $request->image->move(public_path('images'), $imageName);

            $input['image']=$imageName;
        }
        $input['status']=1;
        $product = ProductVariation::create($input);

        return $this->success($input,'Product variation added successfully!');
    }
}
