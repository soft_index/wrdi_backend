<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Review;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    use ApiResponser;

    public function addReview(Request $request)
    {
        $input=$request->validate([
           'product_id' => 'required|int',
           'comment' => 'required'
        ]);
        $user=auth()->user();
        $review=Review::where('user_id',$user->id)->where('product_id',$request->product_id)->first();
        if(!$review)
        {
            $review=new Review();
        }

        $review->product_id=$request->product_id;
        $review->user_id=$user->id;
        $review->rating=$request->rating;
        $review->comment=$request->comment;

        $review->save();

        return $this->success($review,'Review added successfully!.');
    }

    public function getReview()
    {
        $user=auth()->user();
        $review=Review::with('product')->where('user_id',$user->id)->orderBy('created_at','DESC');
        $result=paginate($review,'reviews');
        return $this->success($result,'My reviews!.');
    }

    public function productReviews(Request $request)
    {
        $id=$request->route('id');

        $review=Review::with('user')->where('product_id',$id)->orderBy('created_at','DESC');
        $result=paginate($review,'reviews');
        return $this->success($result,'Product reviews.');
    }
}
