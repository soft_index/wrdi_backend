<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\UserAddress;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserAddressController extends Controller
{
    use ApiResponser;
    public function store(Request $request)
    {
         $request->validate([
            'user_id' => 'required',
            'address' => ['required'],
            'city' => ['required'],
            'zip_code' => ['required'],

        ]);
        $input =$request->all();
        $user = UserAddress::create($input);

        return $this->success($user, 'New address saved successfully!');

    }

    public function details()
    {
        $id =Auth::user()->id;
        $address =UserAddress::where('user_id',$id)->get();
        return $this->success($address, 'Address list!');
    }

    public function address_edit($id)
    {
        $user =Auth::user()->id;
        $address =UserAddress::find($id);

        if (!$user || !$address ) {
            return $this->success( $address,'No address found!');
        }

        return $this->success($address, 'Address get successfully!');
    }


    public function address_update(request $request,$id)
    {

        $user =Auth::user()->id;
        $address =UserAddress::find($id);

        if (!$user || !$address  ) {
            return $this->success( $address,'No address found!');
        }

        $updated = $address->fill($request->all());

        $updated->save();

        if ($updated)
        {
            $success =  $address;
            return $this->success($updated, 'Address updated successfully!');
        }
        else
            return $this->error('Address updated successfully!',401);

    }
    public function address_delete(request $request,$id)
    {
        $user =Auth::user()->id;
        $address =UserAddress::find($id);

        if (!$user || !$address  ) {
            return $this->success( $address,'No address found!');
        }

        $delete=UserAddress::find($id)->delete();

        return $this->success( null,'Address deleted successfully!');


    }
}
