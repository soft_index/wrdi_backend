<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\UserDevice;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class UserDeviceController extends Controller
{
    use ApiResponser;

    public function store(Request $request)
    {
        $input = $request->validate([
            'user_id' => ['required'],
            'device_id' => ['required'],
            'platform' => ['required'],
            'app_version' => ['required'],
            'os_version' => ['required'],

        ]);

        $device = UserDevice::where('user_id', $request->user_id)->first();
        if ($device) {

            $device->device_id=$request->device_id;
            $device->platform=$request->platform;
            $device->app_version=$request->app_version;
            $device->os_version=$request->os_version;
            $device->save();
        } else {

            $device = UserDevice::create($input);
        }

        return $this->success($device, 'Device Added Successfully!');

    }

    public function devices()
    {
        $devices = UserDevice::all();
        if (count($devices) < 1) {
            return $this->success('No Device found!');
        }

        return $this->success($devices, 'Device Added Successfully!');
    }
}
