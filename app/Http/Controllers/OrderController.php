<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderStatus;
use Illuminate\Http\Request;
use DataTables;
class OrderController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Order::with('user','status','items')->orderBy('created_at','desc')->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                    $btn = '<a href="admin/orders/'.$row->id.'" class="edit btn btn-primary btn-sm">View</a>';
                    $btn = $btn.'<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="details" class="edit btn btn-primary  updatestatus" style="background-color:#5A180A !important;">Update Status</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

    }
    public function store(Request $request)
    {

        $order=Order::where('order_no' ,$request->order_no)->first();
        if($order && $request->status)
        {
            $order->status_id=$request->status;
            $order->save();
            return response()->json(['order_no' => $request->order_no,'success'=>$order->status->title]);
        }
        else
            return response()->json(['error'=>'No order found.'],404);



    }
    public function edit($id)
    {
        $order = Order::find($id);
        return response()->json($order);
    }
}
