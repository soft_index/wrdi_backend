<?php

namespace App\Http\Controllers;

use App\Models\RiderOrder;
use Illuminate\Http\Request;

class RiderController extends Controller
{
    public function assign_order(request $request)
    {
        $rider_id=$request->rider_id;
        $order_id=$request->order_id;
        $notes=$request->notes;
        $riderOrder=RiderOrder::where('order_id',$order_id)->first();
        if(!$riderOrder)
        {
            $riderOrder=new RiderOrder();
        }
        $riderOrder->rider_id=$rider_id;
        $riderOrder->order_id=$order_id;
        $riderOrder->notes=$notes;
        $riderOrder->estimated_time=$request->estimated_time;
        $riderOrder->save();

        return json_encode(array('statusCode'=>200,'message'=>"Order Successfully Assign rider."));
    }
}
