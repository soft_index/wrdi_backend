<?php

namespace App\Http\Controllers;

use App\Models\OrderStatus;
use App\Models\StatusNote;
use Illuminate\Http\Request;

use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use DB;
class statusController extends Controller
{

    public function update($id)
    {

        $order = Order::find($id);
        $status=request('status');
        $notes=request('notes');
        if($notes)
        {
            $newNote=StatusNote::where('order_id',$order->id)->where('status_id',$status)->first();
            if(!$newNote)
            {
                $newNote=new StatusNote();
            }
            $newNote->order_id=$order->id;
            $newNote->status_id=$status;
            $newNote->notes=$notes;
            $newNote->save();

        }

        $order->status_id=$status;
        $order->save();
        $current=$order->status->title;
        return json_encode(array('statusCode'=>200,'message'=>"Status Updated.",'data'=>$current));

    }

     public function store()
    {
         $user=Auth::guard('admin')->user();
       $id=request('orderid');
        $order= Order::find($id);

        $order->status_id = OrderStatus::where('title','Cancelled')->value('id');

        $order->save();

        $reason=request('description');


        $cancelled_by=$user->id;

        $order_id=$order->id;

        $order_cancel = array(

                 'order_id'  => $order_id,
                 'cancelled_by'   =>$cancelled_by,
                 'description' => $reason,

                );
                  if(!empty($order_cancel))
                  {

                   DB::table('order_cancels')->Insert($order_cancel);

                  }


        return json_encode(array('statusCode'=>200,'message'=>"Order Cancelled."));
        }


}
