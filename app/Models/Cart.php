<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;
    protected $fillable=['user_id','product_id','quantity','total_amount','description','order_status'];
    protected $appends=['product','variation','total_item_amount'];

    public function getProductAttribute()
    {
        return Product::without('variations')->where('id',$this->product_id)->first();
    }

    public function variations()
    {
        return $this->hasMany(CartVariation::class,'cart_id');
    }
    public function getVariationAttribute()
    {
        return $this->variations()->first();
    }
    public function getTotalItemAmountAttribute()
    {

        $variation=$this->variations()->first();
        if($variation)
        {
            $total=$this->total_amount+$variation->price;
            return $total;
        }
        else
            return $this->total_amount;

    }
}
