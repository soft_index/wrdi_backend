<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartVariation extends Model
{
    use HasFactory;
    protected $appends=['variation'];

    public function getVariationAttribute()
    {
        return ProductVariation::find($this->variation_id)->title;
    }
}
