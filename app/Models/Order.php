<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;
    protected $casts = [
        'order_date' => 'datetime:d/m/Y',
    ];
    use HasFactory;
    protected $fillable=['user_id','net_amount','discount','total_amount','order_date','status_id'];
    protected $appends=['address','order_rider'];
    public function status()
    {
        return $this->belongsTo(OrderStatus::class,'status_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function items()
    {
        return $this->belongsToMany(Cart::class,'order_items','order_id','cart_id');
    }

    public function address()
    {
        return $this->belongsToMany(UserAddress::class,'order_address','order_id','address_id');
    }

    public function getAddressAttribute()
    {
        return $this->address()->first();
    }
    public function orderRider()
    {
        return $this->belongsTo(RiderOrder::class,'order_id');
    }
    public function rider()
    {
        return $this->belongsToMany(Rider::class,'rider_orders','order_id','rider_id');
    }

    public function orderCancel()
    {
        return $this->belongsTo(OrderCancel::class,'order_id');
    }
    public function getOrderRiderAttribute()
    {
        $rider=RiderOrder::where('order_id',$this->id)->first();
        if($rider)
        {
            $data['estimated_time']=$rider->estimated_time;
            $data['name']=$rider->rider->name;
            $data['email']=$rider->rider->phone;
            $data['phone']=$rider->rider->phone;
            return $data;
        }
    }
}
