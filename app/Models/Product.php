<?php

namespace App\Models;

use App\Scopes\HasActiveScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Product extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;
    public $translatedAttributes = ['title', 'description'];

    protected $fillable=['code','price','quantity','image','generic_name',
        'weight','height','width','length'];
    protected $hidden=['created_at','updated_at','translations'];
    protected $with=['variations'];
    protected $appends=['english','arabic','avg'];

    public function variations()
    {
        return $this->hasMany(ProductVariation::class,'product_id');
    }
    public function languages()
    {
        return $this->hasMany(ProductTranslation::class,'product_id');
    }
    public function getEnglishAttribute()
    {
        return $this->translate('en');
    }
    public function getArabicAttribute()
    {
        return $this->translate('ar');
    }
    public function getAvgAttribute()
    {
        $avg=Review::where('product_id',$this->id)->avg('rating');
        if(!$avg)
        {
            $avg= 0;
        }
        return number_format($avg, 1);

    }
}
