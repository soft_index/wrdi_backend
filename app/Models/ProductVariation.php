<?php

namespace App\Models;

use App\Scopes\HasActiveScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductVariation extends Model
{
    use HasFactory;
    protected $table='product_variation';
    protected $fillable=['product_id','title','price','priority','status','image','quantity'];
    protected $hidden=['created_at','updated_at'];


    public function product()
    {
        return $this->belongsTo(product::class,'product_id');
    }
}
