<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RiderOrder extends Model
{
    use HasFactory;
    protected $with=['rider'];
    public function rider()
    {
        return $this->belongsTo(Rider::class,'rider_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class,'order_id');
    }
}
