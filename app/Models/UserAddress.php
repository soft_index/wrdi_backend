<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id','address','latitude','longitude','address_title','notes','name','city','zip_code',
    ];

    protected $hidden = [
        'created_at','updated_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
