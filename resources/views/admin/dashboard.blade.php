<head>
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
</head>
<section class="content">


    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-users"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">New Members</span>
                <span class="info-box-number">{{$users}}</span>
            </div>

        </div>

    </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-shopping-cart"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Products</span>
                    <span class="info-box-number">{{$products}}</span>
                </div>

            </div>

        </div>


        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="ion fa fa-money"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Sales</span>
                    <span class="info-box-number">0</span>
                </div>

            </div>

        </div>


    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-shopping-bag"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Pending Orders</span>
                    <span class="info-box-number">0</span>
                </div>

            </div>

        </div>

    </div>
</section>
    <div class="container" style=" overflow: auto !important; width: 100%">
        <h1 style="text-align: center;">Recent Orders</h1>
        <table class="table table-bordered data-table">
            <thead>
            <tr>
                <th>No</th>
                <th>User</th>
                <th>Date</th>
                <th>Items</th>
                <th>Shipping Address</th>
                <th>Total</th>
                <th>Status</th>
                <th width="100px">Action</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div class="modal modal-info fade" id="ajaxModel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="modelHeading"></h4>
                </div>
                <form id="productForm" name="productForm" >
                    <div class="modal-body">

                        <input type="hidden" name="user" id="id_user">
                        <input type="hidden" name="order_no" id="order_no">

                        <div class="form-group row">

                            <div class="col-sm-12">
                                <label for="name" class="col-sm-2 control-label">Status</label>
                                <select class="form-control selectpicker" data-live-search="true" name="status" id="status">
                                    <option ></option>
                                    @foreach($status as $value)
                                        <option data-tokens="{{$value->title}}" value="{{$value->id}}">{{$value->title}}</option>
                                    @endforeach
                                </select>

                            </div>
                            <div class="col-sm-12">
                                <label>Special Notes</label>
                                <textarea class="form-control" rows="3" id="notes" name="notes" ></textarea>
                            </div>
                        </div>



                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes
                        </button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <script type="text/javascript">
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),

                }
            });

            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('currentorder.index') }}",
                columns: [
                    {data: 'order_no', name: 'id'},
                    {data: 'user.name', name: 'user'},
                    {data: 'order_date', name: 'date'},
                    {data: 'items[, ].product.title', name: 'items'},
                    {data: 'address.address', name: 'address'},
                    {data: 'total_amount', name: 'total_amount'},
                    {data: 'status.title', name: 'status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });



        $('body').on('click', '.updatestatus', function () {
            var id = $(this).data('id');

            $.get("{{ route('currentorder.index') }}" +'/' +id +'/edit', function (data) {
                $('#modelHeading').html(data.order_no);

                $('#saveBtn').val("edit-user");
                $('.modal-body').show();
                $('#id_user').val(data.user_id);
                $('#order_no').val(data.order_no);
                $('#status').val(data.status);
                $('#notes').val(data.notes);
                $('#ajaxModel').modal('show');

            })
        });

        $('#saveBtn').click(function (e) {
            e.preventDefault();
            if(document.getElementById("status").value.length == 0)
            {

                swal({type: 'error',title: 'Please select the status.'});
                return false;
            }

            $.ajax({
                data: $('#productForm').serialize(),
                url: "{{ route('currentorder.store') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {

                    $('#productForm').trigger("reset");
                    $('#ajaxModel').modal('hide');
                    table.draw();
                    swal({type: 'success',title: data.order_no, text:data.success});
                },
                error: function (data) {
                    swal({type: 'error',title: 'Failed', text:data.error});
                    console.log('Error:', data);
                    $('#saveBtn').html('Save Changes');
                }
            });
        });
        $(function () {
            $('select').selectpicker();
        });
        });


    </script>


