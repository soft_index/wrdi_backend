<?php


?>
<section class="invoice">

    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <i class="fa fa-globe"></i> Wrdi.
                <small class="pull-right">Date: {{$order->order_date->format('d/m/Y')}}</small>
                <button type="button" class="btn btn-danger btn-md" data-toggle="modal" data-target="#modal-danger">
                    Cancel
                </button>

                <button type="button" class="btn btn-primary btn-md pull-right" data-toggle="modal" data-target="#modal-info" > Update Status</button>
            </h2>
        </div>

    </div>

    <div class="row invoice-info">
        <div class="col-sm-3 invoice-col">
            User
            <address>
                <strong>{{$order->user ?  $order->user->name : 'none'}}</strong><br>
                Phone: {{$order->user ?  $order->user->phone : 'none'}}<br>
                Email: {{$order->user ?  $order->user->email : 'none'}}
            </address>
        </div>

        <div class="col-sm-3 invoice-col">
            Shipping Details
            <address>
                <strong>{{$order->address ? $order->address->address  : 'none'}}</strong><br>
                City: {{$order->address ? $order->address->city  : 'none'}}<br>
                Zip Code: {{$order->address ? $order->address->zip_code  : 'none'}}
            </address>
        </div>

        <div class="col-sm-3 invoice-col">
            <b>Order NO: {{$order->order_no}}</b><br>
            <br>
            <b>Status:</b><strong> {{$order->status ? $order->status->title : 'none'}}</strong><br>
            @if($cancel)
                <b>Description:</b><strong> {{$cancel ? $cancel->description : 'none'}}</strong><br>
            @endif
            {{--<b>Payment Due:</b> 2/22/2014<br>
            <b>Account:</b> 968-34567--}}
        </div>

        <div class="col-sm-3 invoice-col">
            <b>Rider Details</b><br>
            @if($rider_order)
            <address>
                <strong>{{$rider_order ?  $rider_order->name : 'none'}}</strong><br>
                Phone: {{$rider_order ?  $rider_order->phone : 'none'}}<br>
                Email: {{$rider_order ?  $rider_order->email : 'none'}}
            </address>
                @endif
        </div>

    </div>


    <div class="row">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Qty</th>
                    <th>Product</th>
                    <th>Variation</th>
                    <th>Subtotal</th>
                </tr>
                </thead>
                <tbody>
                @foreach($order->items as $item)
                <tr>
                    <td>{{$item->quantity}}</td>
                    <td>{{$item->product ? $item->product->title : 'none'}}</td>
                    <td>{{$item->variation ? $item->variation->variation : 'none'}}</td>
                  <?php
                    $variation_total=0;
                    if($item->variation)
                        $variation_total=$item->variation->total;
                    ?>
                    <td>{{$item->total_amount + $variation_total}}</td>

                </tr>
                @endforeach


                </tbody>
            </table>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-6">
         {{--   <p class="lead">Payment Methods:</p>
            <img src="../../dist/img/credit/visa.png" alt="Visa">
            <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
            <img src="../../dist/img/credit/american-express.png" alt="American Express">
            <img src="../../dist/img/credit/paypal2.png" alt="Paypal">
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg
                dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
            </p>--}}
        </div>

        <div class="col-xs-6">
           {{-- <p class="lead">Amount Due 2/22/2014</p>--}}
            <div class="table-responsive">
                <table class="table">
                    <tbody><tr>
                        <th style="width:50%">Subtotal:</th>
                        <td>{{$order->net_amount}}</td>
                    </tr>
                    <tr>
                        <th>Discount</th>
                        <td>{{$order->discount}}</td>
                    </tr>
                    <tr>
                        <th>Total:</th>
                        <td>{{$order->total_amount}}</td>
                    </tr>
                    </tbody></table>
            </div>
        </div>

    </div>


    <div class="row no-print">
        <div class="col-xs-12">
            <button type="button" class="btn btn-success btn-md" data-toggle="modal" data-target="#modal-delivery" >Assign Rider</button>
          {{--  <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
            </button>
            <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
                <i class="fa fa-download"></i> Generate PDF
            </button>--}}
        </div>
    </div>
</section>

<div class="modal modal-info fade" id="modal-info" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Update Status</h4>
            </div>
            <form >
                <div class="modal-body">
                    <input type="hidden" name="id" id="order_id" value="{{$order->id}}">

                    <div class="form-group row">

                        <div class="col-sm-12">
                            <select class="form-control selectpicker" data-live-search="true" name="status" id="order_status">

                                @foreach($status as $value)
                                    <option data-tokens="{{$value->title}}" value="{{$value->id}}">{{$value->title}}</option>
                                @endforeach
                            </select>

                        </div>

                        <div class="col-sm-12">
                            <label>Special Notes</label>
                            <textarea class="form-control" rows="3" id="notes" name="notes" ></textarea>
                        </div>
                    </div>



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline" id="update_data">Update</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<div class="modal modal-danger fade in" id="modal-danger" style="display: none; ">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Order Cancel</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="order_id" name="order_id" value="{{$order->id}}">
                <label>Please Describe The Reason</label>
                <textarea class="form-control" rows="2" id="reason" name="reason" required=""></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline" id="cancel">Save</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal modal-success fade" id="modal-delivery" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Assign Order To Rider</h4>
            </div>
            <form >
                <div class="modal-body">
                    <input type="hidden" name="id" id="order_id" value="{{$order->id}}">

                    <div class="form-group row">

                        <div class="col-sm-12">
                            <select class="form-control selectpicker" data-live-search="true" name="rider" id="rider">

                                @foreach($riders as $value)
                                    <option data-tokens="{{$value->name}}" value="{{$value->id}}">{{$value->name}}</option>
                                @endforeach
                            </select>

                        </div>
                        <div class="col-sm-12">
                            <label>Estimated Time</label>
                            <input class="form-control" id="estimated_time" name="estimated_time" >
                        </div>
                        <div class="col-sm-12">
                            <label>Special Notes</label>
                            <textarea class="form-control" rows="3" id="rider_notes" name="notes" ></textarea>
                        </div>
                    </div>



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline" id="assign_rider">Assign</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script type="text/javascript">


    $(function() {
        $('.selectpicker').selectpicker();
    });
</script>
<script type='text/javascript'>

    (function()
    {
        if( window.localStorage )
        {
            if( !localStorage.getItem('firstLoad') )
            {
                localStorage['firstLoad'] = true;
                window.location.reload();
            }
            else
                localStorage.removeItem('firstLoad');
        }
    })();

</script>

<script>

    $(document).on("click", "#update_data", function(e) {
        e.preventDefault();
        if(document.getElementById("order_status").value.length == 0)
        {

            swal({type: 'error',title: 'Please select the status.'});
            return false;
        }

        var url = "{{URL('status/'.$id)}}";




        $.ajax({
            url: url,
            type: "PATCH",
            cache: false,
            data:{
                _token:'{{ csrf_token() }}',

                status: $('#order_status').val(),
                notes: $('#notes').val(),

            },
            cache:false,
            success: function(dataResult){
                dataResult = JSON.parse(dataResult);
                if(dataResult.statusCode==200)
                {

                    swal({type: 'success',title:dataResult.data,text: dataResult.message,showConfirmButton: false});
                    setTimeout(function() {

                        location.reload();

                    }, 1000);




                }


                else{
                    alert("Internal Server Error");
                }
                ;

            }
        });
    });
</script>


<script>
    $(document).ready(function(){
        $(document).on("click", "#cancel", function(e) {
            e.preventDefault();
            var check= $('#reason').val();

            if(document.getElementById("reason").value.length == 0)
            {

                swal({type: 'error',title: 'Description is missing'});
                return false;
            }
            var url = "{{URL('status')}}";
            var id=
                $.ajax({
                    url: url,
                    type: "Post",
                    cache: false,
                    data:{
                        _token:'{{ csrf_token() }}',

                        orderid: $('#order_id').val(),
                        description: $('#reason').val(),

                    },
                    success: function(dataResult){
                        dataResult = JSON.parse(dataResult);
                        if(dataResult.statusCode==200)
                        {

                            swal({type: 'success',text: dataResult.message,showConfirmButton: false});
                            setTimeout(function() {

                                location.reload();
                            }, 1000);




                        }
                        else if(dataResult.statusCode==401)
                        {

                            swal({type: 'false',title: 'Please fill the description.'});



                        }


                        else{
                            alert("Internal Server Error");
                        }
                        ;

                    }
                });
        });
    });

</script>

<script>
    $(document).on("click", "#assign_rider", function(e) {
        e.preventDefault();

        var url = "{{URL('assign-rider')}}";




        $.ajax({
            url: url,
            type: "POST",
            cache: false,
            data:{
                _token:'{{ csrf_token() }}',
                order_id:$('#order_id').val(),
                rider_id: $('#rider').val(),
                notes: $('#rider_notes').val(),
                estimated_time: $('#estimated_time').val(),

            },
            cache:false,
            success: function(dataResult){
                dataResult = JSON.parse(dataResult);
                if(dataResult.statusCode==200)
                {

                    swal({type: 'success',text: dataResult.message,showConfirmButton: false});
                    setTimeout(function() {

                        location.reload();

                    }, 1000);




                }


                else{
                    alert("Internal Server Error");
                }
                ;

            },
            error: function(dataResult){


                console.log(dataResult);


            }

        });
    });
</script>
