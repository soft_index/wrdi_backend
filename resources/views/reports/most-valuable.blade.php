<?php

$amount_total=0;

?>
    <!DOCTYPE html>
<html>
<head>
    <title>Wrdi-Report</title>
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }
        th, td {
            padding: 5px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }
        h5{
            font-weight: normal;
        }

    </style>
</head>

<body>



<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12" style="">
        <form>

            <div class="col-md-3">
                <div class="form-group">
                    <label>Start Date:</label>
                    @if($from)
                        <input type="date" class="form-control pull-right"  name="start" value="{{$from}}" required>

                    @else
                        <input type="date" class="form-control pull-right"  name="start" value="<?php echo date('Y-m-d'); ?>" required>
                @endif
                <!-- /.input group -->
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>End Date:</label>
                    @if($to)
                        <input type="date" class="form-control pull-right"  name="end" value="{{$to}}" required>
                    @else
                        <input type="date" class="form-control pull-right"  name="end" value="<?php echo date('Y-m-d'); ?>" required>
                @endif
                <!-- /.input group -->
                </div>
            </div>
            <div class="col-md-3">


                <button type="submit" class="btn btn-primary btn-md" style="margin-top: 25px;">Search</button>

                <!-- /.input group -->

            </div>
        </form>
    </div>

</div>



<div class="row" style="margin-top: 80px;">
    <div class="col-sm-3"></div>
    <div class="col-sm-5">
        <h2 style="text-align: center; font-weight: bold; margin-bottom: 40px;">Most Valuable Product Sales Report</h2>
    </div>
</div>
</hr>

<table class="table responsive" style="overflow: hidden; width: 100%;">
    <thead>

    <tr>
        <th>Id</th>
        <th>Product</th>
        <th>Total Orders</th>
        <th>Sales</th>

    </tr>
    </thead>

    <tbody>
    @foreach($orders as $index => $order)
        <?php $amount_total+=$order->total;

        ?>
        <tr>
            <td>{{ $loop->index+1 }}</td>
            <td>{{$order->product ? $order->product->title : 'none'}}</td>
            <td>{{$order->total_order}}</td>
            <td>{{$order->total}}</td>
        </tr>
    @endforeach
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <th>{{$amount_total}}</th>



    </tr>
    </tbody>
</table>

</body>
</html>
