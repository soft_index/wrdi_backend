<?php

$amount_total=0;

?>
    <!DOCTYPE html>
<html>
<head>
    <title>Wrdi-Report</title>
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }
        th, td {
            padding: 5px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }
        h5{
            font-weight: normal;
        }

    </style>
</head>

<body>



<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12" style="">
        <form>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Product</label>
                    <select class="form-control pull-right" name="product">
                        <option value="0">Select any product</option>
                        @foreach($products as $product)
                        <option value="{{$product->id}}" {{ ($product->id == $product_id ? "selected":"") }}>{{$product->title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Start Date:</label>
                    @if($from)
                        <input type="date" class="form-control pull-right"  name="start" value="{{$from}}" required>

                    @else
                        <input type="date" class="form-control pull-right"  name="start" value="<?php echo date('Y-m-d'); ?>" required>
                @endif
                <!-- /.input group -->
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>End Date:</label>
                    @if($to)
                        <input type="date" class="form-control pull-right"  name="end" value="{{$to}}" required>
                    @else
                        <input type="date" class="form-control pull-right"  name="end" value="<?php echo date('Y-m-d'); ?>" required>
                @endif
                <!-- /.input group -->
                </div>
            </div>
            <div class="col-md-3">


                <button type="submit" class="btn btn-primary btn-md" style="margin-top: 25px;">Search</button>

                <!-- /.input group -->

            </div>
        </form>
    </div>

</div>



<div class="row" style="margin-top: 80px;">
    <div class="col-sm-3"></div>
    <div class="col-sm-5">
        <h2 style="text-align: center; font-weight: bold; margin-bottom: 40px;">Product Sales Report</h2>
    </div>
</div>
</hr>

<table class="table responsive" style="overflow: hidden; width: 100%;">
    <thead>

    <tr>
        <th>Id</th>
        <th>Product</th>
        <th>No of packets/Quantity</th>
        <th>Date</th>
        <th>Sales</th>

    </tr>
    </thead>

    <tbody>
    @foreach($orders as $index => $order)
        <?php $amount_total+=$order->total_amount;
        $date=date('d-m-Y', strtotime($order->created_at));
        ?>
        <tr>
            <td>{{ $loop->index+1 }}</td>
            <td>{{$order->product ? $order->product->title : 'none'}}</td>
            @if(count($order->variations) > 0)
            <td>{{$order->variations[0]->variation}}</td>
            @else
                <td>{{$order->quantity}}</td>
            @endif
            <td>{{$date}}</td>
            <td>{{$order->total_amount}}</td>
        </tr>
    @endforeach
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th>{{$amount_total}}</th>



    </tr>
    </tbody>
</table>

</body>
</html>
