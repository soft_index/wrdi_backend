<?php

Route::post('/auth/register', 'AuthController@register');

Route::post('/auth/login', 'AuthController@login');

Route::get('get/product','ProductController@getProduct');

Route::get('get/all-status','OrderController@getAllStatus');

Route::get('country/list','CountryController@countryList');
