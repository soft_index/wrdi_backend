<?php

Route::post('add-to-cart','CartController@addtoCart');
Route::get('my-cart','CartController@getCart');
Route::delete('remove-all-items','CartController@removeAll');
Route::delete('remove-item/{id}','CartController@removeitem');
