<?php

Route::post('order-status','OrderController@addOrderStatus');

Route::prefix('user/order')->group(function () {
    Route::post('create','OrderController@createOrder');
    Route::get('get-all','OrderController@getMyOrders');
    Route::get('get/{id}','OrderController@getOrder');
    });

