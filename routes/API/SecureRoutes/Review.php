<?php

Route::post('add-review','ReviewController@addReview')->name('add.review');
Route::get('get/my-review','ReviewController@getReview')->name('get.review');
Route::get('get/product/review/{id}','ReviewController@productReviews')->name('get.product.review');
