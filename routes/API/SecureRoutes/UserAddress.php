<?php


Route::post('add/address', 'UserAddressController@store');
Route::get('get/address', 'UserAddressController@details');
Route::get('edit/address/{id}', 'UserAddressController@address_edit');
Route::put('update/address/{id}', 'UserAddressController@address_update');
Route::delete('delete/address/{id}', 'UserAddressController@address_delete');
