<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/

//        Open routes
Route::group(['laroute' => false, 'namespace' => 'App\Http\Controllers\API', 'as' => 'api.'], function () {
    includeRouteFiles(__DIR__.'/API/OpenRoutes/');
});

//         User secure routes
Route::group([ 'namespace' => 'App\Http\Controllers\API', 'middleware' => ['auth:sanctum']], function () {
    includeRouteFiles(__DIR__.'/API/SecureRoutes/');
});
